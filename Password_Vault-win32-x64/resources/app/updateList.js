let $ = require('jquery')
let fs = require('fs')
let aes = require('crypto-js/aes')
let CryptoJS = require('crypto-js')
let filename = 'vault'
let num = 0
const secretPassword = "abc123"

function addEntry(site, username, password) {
    if (site && username && password) {
        num++
        let updateString = '<tr><td>' + num + '</td><td>' + site + '</td><td>' + username + '</td><td>'
            + aes.decrypt(password, secretPassword).toString(CryptoJS.enc.Utf8) + '</td></tr>'
        $('#vault-table').append(updateString)
    }
}

function loadAndDisplayData() {
    if(fs.existsSync(filename)) {
        let data = fs.readFileSync(filename, 'utf8').split('\n')
        
        data.forEach((vault, index) => {
            let [ site, username, password ] = vault.split(',')
            addEntry( site, username, password)
        })
    } else {
        console.log("File doesn\'t exist. Creating new file.")
        fs.writeFile(filename, '', (err) => {
            if (err)
                console.log(err)
        })
    }
}

loadAndDisplayData()
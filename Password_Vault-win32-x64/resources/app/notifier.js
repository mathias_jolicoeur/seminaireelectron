let $ = require('jquery')
const notifier = require('node-notifier')
const path = require('path')

$('#notify').on('click', () => {
    notifier.notify ({
        title: 'New notification',
        message: 'This is a new notification',
        icon: path.join(__dirname, './image01.png'),
        sound: true,
    })
})
const {app, BrowserWindow, Tray, Menu} = require('electron') 
const url = require('url') 
const path = require('path')  
let win
let iconPath = path.join(__dirname, './image01.png')

function createWindow() { 
    win = new BrowserWindow({webPreferences: {
        nodeIntegration: true 
    }}) 
    win.loadURL(url.format ({ 
        pathname: path.join(__dirname, 'index.html')
    }))
    let trayIcon = new Tray(iconPath)
    let contextMenu = Menu.buildFromTemplate([
        {
            label: 'Open',
            click: function() {
                win.show()
            }
        },
        {
            label: 'Quit',
            click: function() {
                app.isQuitting = true
                app.quit()
            }
        }
    ])
    trayIcon.setContextMenu(contextMenu)
    win.on('close', function(event) {
        win = null
    })
    win.on('minimize', function(event) {
        event.preventDefault()
        win.hide()
    })
    win.on('show', function(event) {
        trayIcon.setHighlightMode('always')
    })
}  

app.on('ready', createWindow) 
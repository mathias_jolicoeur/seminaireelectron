$('#view').on('click', () => {
    win = new BrowserWindow({ width: 500, height: 400, webPreferences: {
        nodeIntegration: true
    }})
    win.loadURL(url.format ({
        pathname: path.join(__dirname, 'list.html'),
        protocol: 'file',
        slashes: true
    }))
})
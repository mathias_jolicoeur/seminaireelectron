const {remote} = require('electron')
const {BrowserWindow} = remote
const url = require('url')
let win

$('#embedded').on('click', () => {
    win = new BrowserWindow({ width: 1000, height: 800, webPreferences: {
        nodeIntegration: true
    }})
    win.maximize()
    win.loadURL(url.format ({
        pathname: path.join(__dirname, 'embedded.html')
    }))
})